import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//引入mint-ui
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css' // 需要单独引入样式文件
Vue.use(MintUI)

//引入iconfont图标库
import './assets/fonts/iconfont.css'
//引入swiper样式
import 'swiper/swiper.min.css'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'   //需要注意的是，样式文件需要单独引入
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
