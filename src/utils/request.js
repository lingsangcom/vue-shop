import axios from 'axios'

//创建一个axios实例对象
var service=axios.create({
    // axios中请求配置有baseURL选项，表示请求URL公共部分
    baseURL:'https://api-hmugo-web.itheima.net/',
    //响应超时时间
    timeout:5000
});

//添加请求拦截器
service.interceptors.request.use((config)=>{
    if (config.method === 'get' && config.params) {
        let url = config.url + '?';
        for (const propName of Object.keys(config.params)) {
          const value = config.params[propName];
          var part = encodeURIComponent(propName) + "=";
          if (value && typeof(value) !== "undefined") {
            if (typeof value === 'object') {
              for (const key of Object.keys(value)) {
                let params = propName + '[' + key + ']';
                var subPart = encodeURIComponent(params) + "=";
                url += subPart + encodeURIComponent(value[key]) + "&";
              }
            } else {
              url += part + encodeURIComponent(value) + "&";
            }
          }
        }
        url = url.slice(0, -1);
        config.params = {};
        config.url = url;
    }
    return config;
});
//添加响应拦截器
service.interceptors.response.use((res)=>{
    return res;
});

export default service;