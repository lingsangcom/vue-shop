import request from '@/utils/request'

//请求轮播图接口
export function getSwiperList(params) {
    return request({
      url: '/api/public/v1/home/swiperdata',
      method: 'get',
      params:params
    })
}
//导航菜单接口
export function getNavList(params) {
  return request({
    url: '/api/public/v1/home/catitems',
    method: 'get',
    params:params
  })
}
//楼层列表接口
export function getFloorList(params) {
  return request({
    url: '/api/public/v1/home/floordata',
    method: 'get',
    params:params
  })
}