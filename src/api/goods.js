import request from '@/utils/request'

//请求商品分类接口
export function getCatsList(params) {
    return request({
      url: '/api/public/v1/categories',
      method: 'get',
      params:params
    })
}
//请求商品列表接口
export function getGoodsList(params) {
  return request({
    url: '/api/public/v1/goods/search',
    method: 'get',
    params:params
  })
}

//请求商品详情接口
export function getGoodsDetail(params) {
  return request({
    url: '/api/public/v1/goods/detail',
    method: 'get',
    params:params
  })
}