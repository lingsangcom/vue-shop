import Vue from "vue";

//新建一个购物车仓库 专门用于保存购物车存放的商品
var shopcart={
    state:{
        itemList:[]
    },
    getters:{
        totalPrice(state){
            return state.itemList.reduce((total,item)=>{
                return total+item.goods_price*item.num
            },0).toFixed(2);
        }
    },
    mutations:{
        add(state,payload){
            var ii=-1;
            state.itemList.forEach((item,index) => {
                if(item.goods_id==payload.goods_id){
                    ii=index;
                }
            });
            //说明点击过
            if(ii!=-1){
                payload.num=++state.itemList[ii].num;
                Vue.set(state.itemList,ii,payload);
            }else{
                //未点击过
                payload.num=1;
                Vue.set(state.itemList,state.itemList.length,payload);
            }
        },
        del(state,payload){
            var ii=-1;
            state.itemList.forEach((item,index) => {
                if(item.goods_id==payload.goods_id){
                    ii=index;
                }
            });
            //查找到下标后进行相应的删除splice
            if(ii!=-1){
                state.itemList.splice(ii,1);
            }
        }
    },
    actions:{}
}

export default shopcart;