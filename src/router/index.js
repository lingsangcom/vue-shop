import Vue from 'vue'
import VueRouter from 'vue-router'

//引入首页组件
import Index from '../views/Index.vue'
//引入商品分类组件
import GoodsCat from '../views/GoodsCat.vue'
//引入商品列表组件
import GoodsList from '../views/GoodsList.vue'
//引入商品详情组件
import GoodsDetail from '../views/GoodsDetail.vue'
//引入购物车组件
import ShopCart from '../views/ShopCart.vue'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location){
  return originalPush.call(this,location).catch(err=>err)
}

Vue.use(VueRouter)

const routes = [
    {
      path:'/',
      redirect:'/index',
      meta:{title:'商城首页'}
    },
    {
      path:'/index',
      name:'index',
      component:Index,
      meta:{title:'商城首页'}
    },
    {
      path:'/goodscat',
      name:'goodscat',
      component:GoodsCat,
      meta:{title:'商品分类'}
    },
    {
      path:'/goodslist',
      name:'goodslist',
      component:GoodsList,
      meta:{title:'商品列表'}
    },
    {
      path:'/goodsdetail',
      name:'goodsdetail',
      component:GoodsDetail,
      meta:{title:'商品详情'}
    },
    {
      path:'/shopcart',
      name:'shopcart',
      component:ShopCart,
      meta:{title:'购物车'}
    }
]

const router = new VueRouter({
  routes
})
/*
* 在路由发生改变时动态设置网页的title标签
  路由钩子函数里获取meta数据
*/
router.beforeEach((to,from,next)=>{
    if(to.meta.title){
       document.title=to.meta.title;
    }
    next();
});
export default router
